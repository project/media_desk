/**
 * @file
 * Javascript file that performs the image selector 
 * file insertions to WYSIWYG fields.
 */
(function ($) {
  // Attaching to Drupal behaviors
  Drupal.behaviors.media_desk_selector_js = {
    attach: function (context, settings) {

      //Show view modes
      $('.field-widget-media-desk-selector .media-widget .insert').once().click(function (event){
        event.preventDefault();
        $(this).siblings('.view-mode-list').toggle();
      });

      //Media token insert
      $('.insert-wysiwyg-image').once().click(function(event) {
        event.preventDefault();

        $(this).parent().hide();
        var file_id = $(this).parent().siblings('.fid').val();
        var view_mode = $(this).attr('data-style');

        $.getJSON('/mediadesk/genimg/' + file_id + '/' + view_mode, function(data) {
          var fields = data.fields.split(/\r\n|\r|\n/g);
          var media_options = {'format' : view_mode};
          var media_attributes = Drupal.media.filter.parseAttributeFields(media_options);

          var element = Drupal.media.filter.create_element(data.element, {
            fid: file_id,
            view_mode: view_mode,
            attributes: media_attributes,
            fields: media_options
          });

          var markup = Drupal.media.filter.getWysiwygHTML(element);
          markup = data.prefix + markup + data.suffix;
          for (var i = 0; i < fields.length; i++) {
            if (typeof Drupal.wysiwyg.instances[fields[i]] !== 'undefined') {
              Drupal.wysiwyg.instances[fields[i]].insert(markup);
            }
          }
        });

      });
    }

  };
}(jQuery));

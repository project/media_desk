<?php
/**
 * @file
 * Creates an easier interface for image workflow.
 */

/**
 * Implements hook_menu().
 */
function media_desk_menu() {
  $items['admin/config/media/media-desk'] = array(
    'title' => 'Media desk',
    'description' => t('Configuration for Media desk.'),
    'file' => 'media_desk.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('media_desk_admin'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['mediadesk/genimg'] = array(
    'page callback' => 'media_desk_genimg',
    'type' => MENU_CALLBACK,
    'access arguments' => array('create files'),
    'delivery callback' => 'media_desk_ajax_callback',
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function media_desk_theme() {
  return array(
    'media_desk_image' => array(
      'render element' => 'element',
    ),
    'media_desk_selector' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function media_desk_field_widget_info() {
  return array(
    'media_desk_image' => array(
      'label' => t('Media desk image'),
      'field types' => array('image'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
      'settings' => array(
        'progress_indicator' => 'throbber',
        'allowed_types' => array('image'),
        'browser_plugins' => array(),
        'allowed_schemes' => array('public', 'private'),
      ),
    ),
    'media_desk_selector' => array(
      'label' => t('Media desk selector'),
      'field types' => array('image'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
      'settings' => array(
        'progress_indicator' => 'throbber',
        'allowed_types' => array('image'),
        'browser_plugins' => array(),
        'allowed_schemes' => array('public', 'private'),
      ),
    )
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */
function media_desk_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  // Use the media widget settings form.
  $form = media_field_widget_settings_form($field, $instance);

  $form['preview_image_style'] = array(
    '#title' => t('Preview image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['preview_image_style'],
    '#description' => t('The preview image will be shown while editing the content.'),
    '#weight' => 15,
  );

  // Todo: validate this field.
  $form['preview_aspect_ratio'] = array(
    '#title' => t('Preview holder aspect ratio'),
    '#description' => t('The aspect ratio of the preview thumbnail image holder in width:height ratio before it contains the preview image.'),
    '#type' => 'textfield',
    '#size' => 6,
    '#maxlength' => 5,
    '#required' => TRUE,
    '#default_value' => $settings['preview_aspect_ratio'] ? $settings['preview_aspect_ratio'] : '4:3',
    '#weight' => 16,
  );

  return $form;
}

/**
 * Implements hook_field_widget_form().
 */
function media_desk_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $element['#attached']['css'] = array(drupal_get_path('module', 'media_desk') . '/media_desk.css');
  $element['#theme'] = $instance['widget']['type'];

  if ($instance['widget']['type'] == 'media_desk_selector') {
    return _media_desk_selector_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
  }
  elseif ($instance['widget']['type'] == 'media_desk_image') {
    return _media_desk_image_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
  }
}

/**
 * Custom field_widget_form implementation for image widget.
 */
function _media_desk_selector_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $element = media_multiselect_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
  $element['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'media_desk') . '/media_desk_selector.js',
    'type' => 'file',
    'weight' => 1250,
  );

  $file = new stdClass();
  $file->type = 'image';

  if (module_exists('media_wysiwyg_view_mode')) {
    $view_modes = media_get_wysiwyg_allowed_view_modes($file);
  }
  else {
    $entity_info = entity_get_info('file');
    foreach ($entity_info['view modes'] as $view_mode => $view_mode_info) {
      $view_modes[$view_mode]['label'] = check_plain($view_mode_info['label']);
    }
  }

  for ($i = 0; $i <= $element['#max_delta']; $i++) {
    $element[$i] += element_info('media');
    $element[$i]['insert'] = array(
      '#type' => 'button',
      '#value' => t('Insert'),
      '#attributes' => array('class' => array('button', 'insert')),
      '#options' => array('fragment' => FALSE, 'external' => TRUE),
      '#weight' => 60,
    );
    $markup = media_desk_view_modes_list($view_modes);
    $element[$i]['view_mode'] = array(
      '#markup' => $markup,
      '#weight' => 65,
    );
  }

  return $element;
}

/**
 * Insert view modes as a select list with each image.
 */
function media_desk_view_modes_list($modes) {
  $markup = '<div class="view-mode-list">';

  foreach ($modes as $mode => $settings) {
    $markup .= '<input type="button"
      name = "field-' . $settings['label'] . '"
      class="insert-wysiwyg-image"
      data-style="' . $mode . '"
      value="' . $settings['label'] .
      '"/>';
  }
  $markup .= '</div>';

  return $markup;
}

/**
 * Custom field_widget_form implementation for image widget.
 */
function _media_desk_image_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $element = media_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);

  // Add process functionality to get a nice preview thumbnail.
  $element += array(
   '#process' => array('media_desk_image_field_widget_process'),
  );
  // If manual crop module is installed, then process the edit field.
  if (module_exists('manualcrop')) {
    $element += array(
     '#after_build' => array('manualcrop_media_element_process'),
    );
  }

  return $element;
}

/**
 * An element #process callback for the image_image field type.
 *
 * Expands the image_image type to include the alt and title fields.
 */
function media_desk_image_field_widget_process(&$element, &$form_state, $form) {
  $m_element = media_element_process($element, $form_state, $form);

  // If manualcrop is enabled we need to rearrange the order of the attached js.
  // Weird way to get it working. Need to get manualcrop to sort it out.
  if (!empty($m_element['#attached']['js'][0]['data']['manualcrop'])) {
    array_push($m_element['#attached']['js'], array_shift($m_element['#attached']['js']));
  }

  // Attach the media desk javascript and jQuery UI droppable to the element.
  $m_element['#attached']['library'] = array(array('system', 'ui.droppable'));
  $m_element['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'media_desk') . '/media_desk.js',
    'type' => 'file',
    'weight' => 1250,
  );

  $instance = field_widget_instance($element, $form_state);
  $settings = $instance['settings'];
  $widget_settings = $instance['widget']['settings'];
  $thumbnail = '';
  $width = 'auto';
  $preview_height = '75px';

  // Add the image preview.
  if ($element['#file'] && $widget_settings['preview_image_style']) {
    $variables = array(
      'style_name' => $widget_settings['preview_image_style'],
      'path' => $element['#file']->uri,
    );

    // Determine image dimensions.
    if (isset($element['#value']['width']) && isset($element['#value']['height'])) {
      $variables['width'] = $element['#value']['width'];
      $variables['height'] = $element['#value']['height'];
    }
    else {
      $info = image_get_info($element['#file']->uri);
      if (is_array($info)) {
        $variables['width'] = $info['width'];
        $variables['height'] = $info['height'];
      }
      else {
        $variables['width'] = $variables['height'] = NULL;
      }
    }
    
    // Get title and alt fields.
    if (isset($element['#value']['field_file_image_title_text']['und'][0]['value'])) {
      $variables['title'] = $element['#value']['field_file_image_title_text']['und'][0]['value'];
    }
    if (isset($element['#value']['field_file_image_alt_text']['und'][0]['value'])) {
      $variables['alt'] = $element['#value']['field_file_image_alt_text']['und'][0]['value'];
    }
    
    $thumbnail = theme('image_style', $variables);
  }

  // Determine the height of preview thumbnail.
  $aspect = explode(':', $widget_settings['preview_aspect_ratio']);
  $preview_width = round(($preview_height * $aspect[0]) / $aspect[1]) . 'px';

  $m_element['preview'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="' . $instance['field_name'] . '" class="field_identifier"></div><div class="preview launcher" style="height: ' . $preview_height . '; width: ' . $preview_width . ';"><div class="media-item"><div class="media-thumbnail">',
    '#suffix' => '</div></div></div>',
    '#markup' => $thumbnail,
  );

  return $m_element;
}

/**
 * Implements theme_hook().
 */
function theme_media_desk_image($variables) {
  //Just themeing with CSS atm. If required in future we can theme this widget.
}

/**
 * Theming the media_desk_selector field widget.
 */
function theme_media_desk_selector($variables) {
  $element = $variables['element'];
  $output = '';
  $list = array();

  $items = array();
  foreach (element_children($element) as $key) {
    if ($key === 'add_more') {
      $add_more_button = &$element[$key];
    }
    else {
      $items[] = &$element[$key];
    }
  }
  usort($items, '_field_sort_items_value_helper');

  // Add the items.
  foreach ($items as $key => $item) {
    unset($item['title']);
    unset($item['select']);
    unset($item['_weight']);
    $item['edit'] = str_replace($item['#field_name'], '', $item['edit']);
    $list['items'][] = drupal_render($item);
  }

  $output = '<div class="form-item">';
  $output .= theme('item_list', $list);
  $output .= $element['#description'] ? '<div class="description">' . $element['#description'] . '</div>' : '';
  $output .= '<div class="clearfix"></div><div>' . drupal_render($add_more_button) . '</div>';
  $output .= '</div>';

  return $output;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function media_desk_form_file_entity_edit_alter(&$form, &$form_state) {
  if (module_exists('manualcrop')) {
    $form['media_desk_js'] = array('#markup' => "<script>jQuery('.manualcrop-image').load(function(){jQuery('#file-entity-edit a.manualcrop-style-thumb').trigger('onmousedown');});</script>");
  }
}

/**
 * Implements hook_manualcrop_supported_widgets().
 */
function media_desk_manualcrop_supported_widgets() {
  return array(
    'media_multiselect' => array('thumblist', 'inline_crop', 'instant_crop'),
    'media_desk_image' => array('thumblist', 'inline_crop', 'instant_crop'),
    'media_desk_selector' => array('thumblist', 'inline_crop', 'instant_crop'),
  );
}

/**
 * Generate image URI using ID.
 */
function media_desk_genimg($fid = NULL, $view_mode = NULL) {
  if (!empty($fid) && is_numeric($fid) && !empty($view_mode)) {
    $file = file_load($fid);
    $fv = file_view($file, $view_mode);
    $style = $fv['file']['#image_style'];

    $uri = image_style_url($style, $file->uri);
    $elr = array(
      'element' => '<img src="' . $uri . '"/>',
      'prefix' => '',
      'suffix' => '',
      'title' => $file->title,
      'alt' => $file->alt,
      'fields' => variable_get('media_desk_wysiwyg_fields', 'edit-body-und-0-value'),
    );

    // Allow other modules to alter the element array.
    drupal_alter('media_desk_element', $elr, $file);

    return json_encode($elr);
  }
  else {
    watchdog('media_desk', 'Image style/id is empty/invalid.', WATCHDOG_ERROR);
    return;
  }
}

/**
 * The delivery callback function.
 */
function media_desk_ajax_callback($page_callback_result) {
  // Only render content
  echo $page_callback_result;
  // Perform end-of-request tasks.
  drupal_page_footer();
}

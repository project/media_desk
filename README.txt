Description
-----------
This module is an extension to the Drupals Media module and together with other 
modules like plupload, manualcrop, multiselect it provides a slick drag-and-drop interface
 to work with many images in an article.

This is useful for large news sites or portals that have many image formats and
need to cut-down the amount of time used in adding, editing and managing images 
in articles.


Dependencies
------------
1. Media
2. Plupload
3. File Entity

If you wish to use the insert images into your WYSIWYG fields then you would additionally
need the wysiwyg module as well.


Recommended modules
-------------------
1. Wysiwyg
2. EXIF custom


Installation
------------
To install, copy the media_desk directory and all its contents to your modules
directory.

To enable this module, visit Administration -> Modules, and enable Media Desk.


Usage
-----
This module primarily provides two types of image field widgets. The "Media desk 
selector" and the "Media desk image". 

Media desk selector:
This image field widget is a simple element that allows you to upload multiple
images to your article. It displays them horizontally in a neat row thereby
giving you a clean overview of all the images you would want to use in your 
article. From here you can drag and drop these images onto any "media desk 
image" fields. You would ideally keep the cardinality of this field to unlimited.

Media desk image:
This image field widget is the actual image field itself. You can use these 
fields as a stand-alone without the "Media desk selector" element. If you have
the "Media desk selector" element then you can drag images from the selector and
drop them into these fields.

It is highly recommended to arrange all your "Media desk image" fields one after
another so that they appear neatly in a horizontal row for best overview 
purpose. This module modifies the theme CSS to arrange these fields neatly.

The "Media desk selector" element can be placed above or below all your "Media 
desk image" elements.   


Crop functionality:
-------------------
The Media Desk module now comes packaged with the manualcrop module that gives
it the cropping functionality. You need to enable the Media Desk Crop module 
and configure atleast one image style to use cropping and then configure your
media desk image fields to enable cropping. Read more about this in the 
media_desk/manualcrop/README.txt file.

You should also choose an appropriate image style for the "Preview image style"
option when configuring your media desk image field. The chosen image style 
should have the crop effect added to it so that you get to see the cropped 
image thumbnail after cropping. This thumbnail however does not update at once
but is updated only after you save the node.


Image insert into WYSIWYG functionality:
----------------------------------------
Media Desk has implemented a new feature that allows images to be inserted
into WYSIWYG fields when using CKeditor. This currently only works when 
using the "wysiwyg" module and the CKeditor library (version 4). Instructions
on how to install WYSIWYG and CKeditor can be found with the wysiwyg module.

There are however a few caveats here as the Media + Wysiwyg combination is
not yet stable and you need a few patches to get them working properly.

Patches Needed:
  1. First the media module needs this patch to work properly with the wysiwyg
     module:
     https://drupal.org/files/media-wysiwyg-broken-2067063-138_0.patch

  2. Then the wysiwyg module needs this patch to identify ckeditor library 
     version 4:
     https://drupal.org/files/wysiwyg-ckeditor-4.1853550.136.patch

You should also note that this feature does not work if you are running 
drupal under a folder and not directly under your site URL.
For example, if your site is www.yoursite.com and if you are running
drupal at www.yoursite.com/drupal/ then this feature might not work.
We are currently working to fix this issue.

Configuration:
  1. To configure this feature to work, go to admin/config/media/media-desk 
     and specify the name of the WYSIWYG field where you wish to insert  
     images. This is usually your body field and the default here should be 
     correct unless you have setup internationalization and configured a 
     language for your field in which case you need to find out the ID 
     of the field and insert it here.
     We are working to find a way to get rid of this configuration. Probably
     a drag-drop feature in the future will fix this work-around.

  2. The "Convert Media tags to markup" filter must be enabled for the 
     format you use in order to use the Media browser WYSIWYG button. This
     can be done under admin/config/content/formats/

  3. Enable the "media browser" button under the "Buttons and Plugins" tab 
     in your wysiwyg profile found under: 
     admin/config/content/wysiwyg/profile/filtered_html/edit
     This is required for the media token to markup filter to work. The
     button itself is otherwise not required for any other purpose. We
     will find a way to remove this requirement in the next version.

  3. It's highly recommended to enable the "Media WYSIWYG view mode" 
     sub-module and configure which view modes you would like to use for
     the Image type. This limits unnecessary views modes from showing
     up in the select menu giving users a cleaner overview.

  4. You can setup your own complex view modes to use this feature to 
     get images the way you want to appear inside the WYSIWYG fields.
     Media desk will automatically append captions to the images.
     For example: view modes for different sizes of images needed and 
     for different alignments in the body area.

Usage:
  1. You upload images using the "media desk selector" field and all the 
     uploaded images will be displayed horizontally. These images can be 
     dragged and dropped into "media desk image" elements.

  2. Each of the horizontally lined images have their own edit, remove 
     and insert buttons. Use the edit button to add captions for images.

  3. Use the insert button when you need to insert images into your
     WYSIWYG field.

  4. Clicking the insert button brings up a box of all your available
     image view modes. Choose a view mode from here and an image
     using that view mode will be inserted into your WYSIWYG field.

  5. You can then right click on the inserted image to perform 
     additional functions provided by the CKeditor library.


General Troubleshooting
-----------------------
1. There have been issues using jQuery version 1.8 or above with the media 
module. This really is not a problem with the media desk module but rather
something media module needs to take a look at. Version 1.7 and below should
work fine with this module.

2. Make sure the "Media desk selector" widget always has cardinality greater
than 1. This means the number of values users can enter for this field should
be greater than 1.

3. The 7.x-1.0-alpha3 release integrates the media multiselect and manualcrop 
module into the media desk module. Therefore the media multiselect and 
manualcrop modules are not required anymore. If you have them enabled please 
disable it to avoid conflicts.

4. There are also a bunch of other patches which helps to solve many issues
with the media module. Here are list of patches most people are using to fix
issues they have with media module.

- https://drupal.org/files/attach-media-css-to-thumbnail-preview-2120853-1.patch
- https://drupal.org/files/correct-internet-media-add-form-help-text-2084299-2.patch
- https://drupal.org/files/make-validation-error-consistent-with-core-1941700-3.patch
- https://drupal.org/files/size-media-browser-dialog-1972628-3.patch
- https://drupal.org/files/media-js-error-on-wysiwyg-with-multi-field-1630288-21.patch

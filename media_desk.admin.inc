<?php
/**
 * @file
 * Administrative page callbacks for the media desk module.
 */

/**
 * General configuration form for controlling the media desk behaviour.
 */
function media_desk_admin() {
  $form = array();

  $form['media_desk_clone_file'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clone files on reuse'),
    '#default_value' => variable_get('media_desk_clone_file', '0'),
    '#return_value' => 1,
    '#description' => t("If checked, re-using an image will automatically clone the file. This is to ensure that images can be reused with different captions and crop parameters per instance."),
  );

  $form['media_desk_wysiwyg_fields'] = array(
    '#type' => 'textarea',
    '#title' => t('Field ID of WYSIWYG fields where images can be inserted.'),
    '#default_value' => variable_get('media_desk_wysiwyg_fields', 'edit-body-und-0-value'),
    '#description' => t('Speciy field IDs of WYSIWYG fields where images can be inserted. One on each line.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
